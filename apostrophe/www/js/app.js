// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var db = null;
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.routes', 'starter.services'])

    .constant('settings', {
        HOST: 'https://apostrophe.ua',
        dbname: 'rss.db',
        db_delim: '*',
        API: {
            getCategories: '/site/getArticleCategories',
            getMain: '/site/getMain',
            getArticles: '/site/getArticles',
            getArticlesByAuthor: '/site/getArticlesByAuthor',
            getNews: '/site/getNews',
            getGalleries: '/site/getAlbums',
            getMaterialArticle: '/site/getMaterialArticle',
            getMaterialNews: '/site/getMaterialNews',
            getMaterialGallery: '/site/getMaterialAlbum'
        },
        HEADERS: {
            title: 'Апостроф',
            prefix: 'Апостроф<span class="dot">&#149;</span>',
            menu: 'Меню',
            main: 'Главная',
            articles: 'Статьи',
            author: 'Автор',
            news: 'Новости',
            interview: 'Интервью',
            opinion: 'Мнение',
            gallery: 'Фотовзгляд',
            tv: 'TV',
            favorites: 'Избранное',
            options: 'Опции'
        },
        LABELS: {
            title: 'Апостроф',
            interview: 'Интервью',
            opinion: 'Мнение',
            gallery: 'Фотовзгляд',
            tv: 'Апостроф TV',
            lime: 'Лайм',
            article: 'Статья',
            news: 'Новость',
            gallery: 'Фотовзгляд'
        },
        LANG: {
            mainnews: 'Главные новости',
            allauthor: 'Все записи автора',
            news: 'Новости',
            readmore: 'Читайте также',
            toupdate: 'Обновить',
            loading: 'Идёт загрузка',
            uploading: 'Идет загрузка, обновите страницу',
            updating: 'На серверах Апостроф идет обновление',
            offline: 'Подключитесь к интернету и обновите',
            turnforupdate: 'Потяните вниз для обновления',
            offvideo: 'Видео не будут показаны в оффлайн режиме',
            toshare: 'Поделиться',
            tosharefb: 'Поделиться',
            totweet: 'Твитнуть',
            shareSlide: 'Поделиться слайдом',
            slideFromB: 'Слайд: ',
            slideFromE: ' из '
        }
    })

    .run(function ($ionicPlatform, $localstorage, $cordovaSQLite, settings) {
        $localstorage.set('FirstRun', true);
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }

            if (window.StatusBar) {
                StatusBar.style(2);
            }
            if ($localstorage.get('DarkMode') == undefined) {
                $localstorage.set('DarkMode', false);
            }
            if ($localstorage.get('TextSize') == undefined) {
                $localstorage.set('TextSize', '100');
            }

            db = $cordovaSQLite.openDB({name:settings.dbname, iosDatabaseLocation: 'default'});
            //$cordovaSQLite.execute(db, "DROP TABLE IF EXISTS pages;");
            //$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS pages(id INTEGER PRIMARY KEY AUTOINCREMENT, page TEXT, block TEXT NOT NULL, json_materials TEXT);");
            //$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS pages(id INTEGER PRIMARY KEY AUTOINCREMENT, block TEXT UNIQUE, json_materials TEXT);");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS pages(pageblock TEXT PRIMARY KEY UNIQUE, json_materials TEXT);");
            //$cordovaSQLite.execute(db, "CREATE UNIQUE pages_block ON pages(page+block);");


            //$cordovaSQLite.execute(db, "DROP TABLE IF EXISTS materials;");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS materials(material TEXT UNIQUE, id INTEGER NOT NULL, model TEXT NOT NULL, title TEXT NOT NULL, img TEXT NOT NULL, read BOOLEAN DEFAULT 0, fav BOOLEAN DEFAULT 0);");

            $cordovaSQLite.execute(db, "create table if not exists article(link_id text unique, feed_id int not null, json_article text not null, fav boolean not null,title text not null,description text not null);");
        });
    })

    .config(function ($ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.navBar.alignTitle('center');
    })

    .filter('to_trusted', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }]);

