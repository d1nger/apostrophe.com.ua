angular.module('starter.routes', [])

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                cache: false,
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                //controller: 'AppCtrl'
                controller: 'MenuCtrl'
            })

            .state('app.favorites', {
                cache: false,
                url: '/fav',
                views: {
                    'tab-fav': {
                        templateUrl: 'templates/favorites.html',
                        controller: 'favoritesCtrl'
                    }
                }
            })

            .state('app.parametre', {
                url: '/param',
                views: {
                    'tab-param': {
                        templateUrl: 'templates/parametre.html',
                        controller: 'parametreCtrl'
                    }
                }
            })

            .state('app.article', {
                cache: false,
                url: '/article/:urlArticle',
                views: {
                    'tab-new': {
                        templateUrl: 'templates/article.html',
                        controller: 'articleCtrl'
                    }
                }
            })

            .state('app.articleF', {
                cache: false,
                url: '/articleF/:urlArticle',
                views: {
                    'tab-fav': {
                        templateUrl: 'templates/article.html',
                        controller: 'articleCtrl'
                    }
                }
            })

            .state('app.New', {
                cache: false,
                url: '/New/:RSSID',
                views: {
                    'tab-new': {
                        templateUrl: 'templates/New.html',
                        controller: 'NewCtrl',
                        resolve: {
                            RSS: function ($stateParams, RSSService) {
                                return RSSService.getRSS($stateParams.RSSID)
                            }
                        }
                    }
                }
            })



            /* Added by Alex M */
            .state('app.getMaterial', {
                cache: false,
                url: '/material/:urlMaterial',
                views: {
                    'tab-new': {
                        templateUrl: function ($stateParams) {
                            return 'templates/' + $stateParams.type + '_material.html';
                        },
                        controller: 'getMaterialCtrl',
                        resolve: {
                            DATA: function ($stateParams, TypeService) {
                                return TypeService.getDATA($stateParams.type)
                            }
                        }
                    }
                },
                params: {
                    type: ':type',
                    id: ':id',
                    header: ':header'
                }
            })

            .state('app.Lenta', {
                cache: false,
                url: '/Lenta/Main/:newspage',
                views: {
                    'tab-new': {
                        templateUrl: 'templates/LentaMain.html',
                        controller: 'getLentaMainCtrl',
                        resolve: {
                            DATA: function ($stateParams, AliasService) {
                                return AliasService.getDATA('Main')
                            }
                        }
                    }
                }
            })

            .state('app.getArticles', {
                cache: false,
                url: '/Lenta/Articles/:category',
                views: {
                    'tab-new': {
                        templateUrl: 'templates/LentaArticles.html',
                        controller: 'getLentaArticlesCtrl',
                        resolve: {
                            DATA: function ($stateParams, AliasService) {
                                return AliasService.getDATA('Articles')
                            }
                        }
                    }
                },
                params: {
                    page: ':page',
                    header: ':header'
                }
            })

            .state('app.getArticlesByAuthor', {
                cache: false,
                url: '/Lenta/ArticlesByAuthor/:author',
                views: {
                    'tab-new': {
                        templateUrl: 'templates/LentaArticlesByAuthor.html',
                        controller: 'getLentaArticlesByAuthorCtrl',
                        resolve: {
                            DATA: function ($stateParams, AliasService) {
                                return AliasService.getDATA('ArticlesByAuthor')
                            }
                        }
                    }
                },
                params: {
                    page: ':page',
                    header: ':header',
                    id: ':id'
                }
            })
            
            .state('app.getNews', {
                cache: false,
                url: '/Lenta/News/:category',
                views: {
                    'tab-new': {
                        templateUrl: 'templates/LentaNews.html',
                        controller: 'getLentaNewsCtrl',
                        resolve: {
                            DATA: function ($stateParams, AliasService) {
                                return AliasService.getDATA('News')
                            }
                        }
                    }
                },
                params: {
                    page: ':page',
                    header: ''
                }
            })

            .state('app.getGalleries', {
                cache: false,
                url: '/Lenta/Galleries/',
                views: {
                    'tab-new': {
                        templateUrl: 'templates/LentaGalleries.html',
                        controller: 'getLentaGalleriesCtrl',
                        resolve: {
                            DATA: function ($stateParams, AliasService) {
                                return AliasService.getDATA('Galleries')
                            }
                        }
                    }
                },
                params: {
                    page: ':page',
                    header: ':header'
                }
            })

        ;
        // if none of the above states are matched, use this as the fallback
        //$urlRouterProvider.otherwise('/app/New/1');
        //$urlRouterProvider.otherwise('/app/Lenta/Main');
        $urlRouterProvider.otherwise('/app/Lenta/Main/0');
    });
