﻿angular.module('starter.controllers', [])

    .factory('FavoriteService', function($rootScope, settings, $cordovaSQLite) {

        var sharedService = {};

        sharedService.showStar = 0;
        sharedService.material = {id: null};

        sharedService.prepForBroadcast = function(showStar) {
            if (showStar > -1) {
                sharedService.showStar = showStar;
            }
            this.broadcastItem();
        };

        sharedService.broadcastItem = function() {
            $rootScope.$broadcast('handleBroadcastFavorite');
        };

        sharedService.getFavoriteStatus = function (model, id) {
            var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default' });
            var query = "SELECT * FROM materials WHERE material=?";
            $cordovaSQLite.execute(dbm, query, [id+settings.db_delim+model]).then(function (res) {
                if (res.rows.length > 0) {
                    sharedService.material.favorite =  res.rows.item(0).fav;
                    sharedService.material.read = res.rows.item(0).read;
                }
                sharedService.prepForBroadcast(1);
            }, function (err) {
                console.log('getFavoriteStatus : articleCtrl :: err=', err);
            });
        };

        sharedService.toggleFavorite = function () {
            var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default' });
            var query = "INSERT OR REPLACE INTO materials(material, id, model, title, img, fav) VALUES (?,?,?,?,?,?);";
            $cordovaSQLite.execute(dbm, query, [
                sharedService.material.id + settings.db_delim + sharedService.material.model,
                sharedService.material.id,
                sharedService.material.model,
                sharedService.material.title,
                sharedService.material.img || '',
                sharedService.material.favorite ? 0 : 1
            ], function () {
            }, function (error) {
                console.log('toggleFavorites : insert error=', error);
            });
        };
        return sharedService;
    })

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout, $localstorage) {
        console.log('AppCtrl');
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        }
    })

    .controller('MenuCtrl', function ($scope, $localstorage, $ionicPlatform, settings, FavoriteService, $http, $state, $q, $cordovaNetwork, $cordovaDevice, $cordovaSQLite, $ionicLoading) {
        console.log('MenuCtrl :: begin : $state=', $state);

        $scope.showStar = FavoriteService.showStar;
        $scope.material = FavoriteService.material;

        $scope.LABELS = settings.LABELS;
        $scope.HEADERS = settings.HEADERS;
        $scope.article_cats = {
            show: false,
            items: [{name: 'all', title: 'Все статьи'}]
        };

        var init = function () {
            $ionicPlatform.ready(function () {
                console.log('MenuCtrl :: $ionicPlatform.ready = yes !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                //console.log('MenuCtrl :: $ionicPlatform.ready : $cordovaDevice.getPlatform()=', $cordovaDevice.getPlatform());
                //$scope.internet_fail = checkOffline();
                if (window.localStorage['FirstRun'] !== undefined) {
                    if (JSON.parse(window.localStorage['FirstRun']) == true && $scope.internet_fail == false) {
                        $scope.refreshed = false;
                        window.localStorage['FirstRun'] = false;
                    }
                }
                if (window.localStorage[name] !== undefined && $scope.refreshed == true) {
                    $scope.server_fail = false;
                    $scope.entries = JSON.parse(window.localStorage[name]);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                else {
                    $scope.refreshed = true;

                    $http.get(settings.HOST + settings.API.getCategories)
                        .success(function (data) {
                            console.log('MenuCtrl :: $http.get=', data);

                            $scope.server_fail = false;
                            window.localStorage['FirstRUn'] = 'false';

                            $scope.article_cats.items = [];
                            for (var key in data) {
                                $scope.article_cats.items.push(data[key]);
                            }
                            //$scope.articles.items.push({name: 'loading', title: '<img src="img/logo_a40.png">'});
                            console.log('$ionicPlatform=', $ionicPlatform);
                            // console.log('ionic.Platform.isAndroid()=', ionic.Platform.isAndroid());
                            // if($cordovaDevice.getPlatform()=="Android")﻿{
                            // if (ionic.Platform.isAndroid()﻿ {
                            //     console.log('isAndroid=');
                            //     //Works on android but not in iOS
                            //     //var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default'});
                            //     //var dbm = window.sqlitePlugin.openDatabase({ name: settings.dbname, androidDatabaseImplementation: 2, androidLockWorkaround: 1 });
                            // }
                            // else {
                            //     console.log('noAndroid=');
                            //     // Works on iOS
                            //     //var dbm = window.sqlitePlugin.openDatabase({ name: settings.dbname, location: 2, createFromLocation: 1});
                            //     //var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default'});
                            // }

                            var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default'});
                            // $scope.entries = data.rss.channel.item;
                            // window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
                            // for (var i = 0; i < data.rss.channel.item.length; i++) {
                            //     $scope.progressing++;
                            //     getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
                            //         var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";
                            //
                            //         dbm.transaction(function (tx) {
                            //                 tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {
                            //
                            //                 }, function (error) {
                            //
                            //                 })
                            //             },
                            //             function (error) {
                            //
                            //             }, function (resp) {
                            //
                            //             });
                            //
                            //     })
                            // }
                            $ionicLoading.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                        })
                        .error(function (error) {
                            console.log('MenuCtrl :: $http.get : error=', error);
                            $ionicLoading.hide();
                            //If it fails to load: showing Connect Internet alert
                            $scope.server_fail = true;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                }
            })
        };

        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.toggleGroup = function (group) {
            group.show = !group.show;
        };
        $scope.isGroupShown = function (group) {
            return group.show;
        };
        $scope.ChooseItem = function (item) {
            FavoriteService.prepForBroadcast(0);
            $state.go("app."+item.route, {page: 0, category: 'category', header: 'header'});
        };
        $scope.Choose = function (item) {
            FavoriteService.prepForBroadcast(0);
            $state.go("app.getArticles", {page: 0, category: item.category, header: item.header});
        };
        $scope.openGalleries = function (item) {
            FavoriteService.prepForBroadcast(0);
            $state.go("app.getGalleries", {page: 0, header: item.header, label: item.label});
        };
        $scope.$on('handleBroadcastFavorite', function() {
            $scope.showStar = FavoriteService.showStar;
            $scope.material = FavoriteService.material;
        });
        $scope.toggleFavorites = function () {
            FavoriteService.toggleFavorite();
            FavoriteService.material.favorite = FavoriteService.material.favorite ? 0 : 1;
            FavoriteService.broadcastItem();
        };

        init();
    })

    .controller('getLentaMainCtrl', function ($http, $scope, $state, settings, FavoriteService, $q, $cordovaNetwork, $cordovaSQLite, $ionicPlatform, $localstorage, DATA, $ionicLoading) {
        console.log('getLentaMainCtrl :: begin : $state=', $state);

        $scope.API = DATA;
        $scope.refreshed = true;
        $scope.server_fail = false;
        $scope.internet_fail = false;
        $scope.LANG = settings.LANG;
        $scope.LABELS = settings.LABELS;
        $scope.header = settings.HEADERS.prefix + settings.HEADERS.main;
        $scope.progressing = 0;
        $scope.top_news = null;
        if ($scope.last_news == undefined) {
            $scope.last_news = [];
        }
        FavoriteService.prepForBroadcast(0);

        var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default' });
        var name = "entries" + DATA.alias;
        var checkOffline = function () {
            return $cordovaNetwork.isOffline();
        };
        var setDataToBD = function (data, prefix, delim) {
            for (var block in data) {
                if (data.hasOwnProperty(block) && block != 'last_news') {
                    var materials = JSON.stringify(data[block]);
                    var query = "INSERT OR REPLACE INTO pages(pageblock, json_materials) VALUES (?,?);";
                    $cordovaSQLite.execute(dbm, query, [prefix+delim+block, materials], function () {
                        console.log("setDataToBD : insert success");
                    }, function (error) {
                        console.log('setDataToBD : insert error=', error);
                    });
                }
            }
        };
        var getDataFromBD = function (prefix, delim) {
            $ionicPlatform.ready(function () {
                var query = "SELECT * FROM pages WHERE pageblock=? OR pageblock=? OR pageblock=? OR pageblock=?";
                $cordovaSQLite.execute(dbm, query, [
                        prefix+delim+'top_actuality_article',
                        prefix+delim+'actuality_article',
                        prefix+delim+'top_news',
                        prefix+delim+'last_news'
                    ]).then(function (res) {
                        for (var pblock = 0; pblock < res.rows.length; pblock++) {
                            if (res.rows.item(pblock).hasOwnProperty('pageblock') && res.rows.item(pblock).hasOwnProperty('json_materials')) {
                                switch (res.rows.item(pblock).pageblock) {
                                    case prefix+delim+'top_actuality_article':
                                        $scope.top_actuality_article = JSON.parse(res.rows.item(pblock).json_materials);
                                        break;

                                    case prefix+delim+'top_news':
                                        $scope.top_news = JSON.parse(res.rows.item(pblock).json_materials);
                                        break;

                                    case prefix+delim+'actuality_article':
                                        $scope.actuality_article = JSON.parse(res.rows.item(pblock).json_materials);
                                        break;

                                    case prefix+delim+'last_news':
                                       // $scope.last_news.push(JSON.parse(res.rows.item(pblock).json_materials));
                                        break;

                                    default:
                                        console.log('res.rows.item('+pblock+')=',res.rows.item(pblock));
                                    break;
                                }
                            }
                        }
                }, function (err) {
                    console.log('getDataFromBD : articleCtrl :: err=', err);
                });
            });
        };
        var init = function () {
            $ionicPlatform.ready(function () {
                var aid = DATA.id;

                $scope.internet_fail = checkOffline();
                if (window.localStorage['FirstRun'] !== undefined) {
                    if (JSON.parse(window.localStorage['FirstRun']) == true && $scope.internet_fail == false) {
                        $scope.refreshed = false;
                        window.localStorage['FirstRun'] = false;
                    }
                }
                if (window.localStorage[name] !== undefined && $scope.refreshed == true) {
                    $scope.server_fail = false;
                    $scope.entries = JSON.parse(window.localStorage[name]);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                else {
                    $scope.refreshed = true;

                    //$scope.last_news = [];
                    $http.get(DATA.link, {params: {"page": $state.params.newspage}})
                        .success(function (data) {
                            console.log('getLentaMainCtrl :: $http.get=', data);

                            $scope.server_fail = false;
                            window.localStorage['FirstRUn'] = 'false';
                            $scope.top_actuality_article = data.top_actuality_article;
                            $scope.top_news = data.top_news;
                            $scope.actuality_article = data.actuality_article;
                            $scope.last_news.push(data.last_news);

                            setDataToBD(data, DATA.alias, '*');
                            /*var query = "DELETE FROM pages WHERE page=? AND block=?";
                            var query = "DELETE FROM pages";
                            $cordovaSQLite.execute(dbm, query, []).then(function(results) {
                                alert("delete success");
                            }, function (err) {
                                console.error(err);
                            });
                            */
                            /*
                             var query = "DELETE FROM pages WHERE page=? AND block=?";
                             dbm.transaction(function (tx) {
                             console.log('DELETE : DATA.alias=', DATA.alias);
                             console.log('DELETE : block=', block);
                             tx.executeSql(query, [DATA.alias, block], function () {
                             alert("delete success");
                             }, function (error) {
                             console.log('delete error=', error);
                             })
                             },
                             function (error) {
                             console.log('delete error=', error);
                             }, function (resp) {
                             console.log('delete resp=', resp);
                             });
                             */
                            /*for (var j = 0; j < data.length; j++) {
                                console.log('data.item[j]=', data.item[j]);
                                //      data.rss.channel.item[j].read = 'false';
                                //      data.rss.channel.item[j].theDate = Date.parse(data.rss.channel.item[j].pubDate);
                            }*/
                            /*$scope.entries = data.rss.channel.item;
                            window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
                            for (var i = 0; i < data.rss.channel.item.length; i++) {
                                $scope.progressing++;
                                getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
                                    var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";

                                    dbm.transaction(function (tx) {
                                            tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {

                                            }, function (error) {

                                            })
                                        },
                                        function (error) {

                                        }, function (resp) {

                                        });

                                })
                            }*/
                            $ionicLoading.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        })
                        .error(function (error) {
                            $ionicLoading.hide();
                            //If it fails to load: showing Connect Internet alert
                            $scope.server_fail = true;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                        // $http.get("http://apostrophe.ua/api/parser.php", { params: { "rss_url": $scope.RSS.link } })
//     .success(function (data) {
//         console.log('data='+data);
//         $scope.server_fail = false;
//         window.localStorage['FirstRUn'] = 'false';
//         // for (var j = 0; j < data.rss.channel.item.length; j++) {
//         //      data.rss.channel.item[j].read = 'false';
//         //      data.rss.channel.item[j].theDate = Date.parse(data.rss.channel.item[j].pubDate);
//         // };
//
//         // console.log('$cordovaDevice.getPlatform()='+$cordovaDevice.getPlatform());
//         // if($cordovaDevice.getPlatform()=="Android")﻿{
//         //     // Works on android but not in iOS
//         //     var dbm = $cordovaSQLite.openDB({ name: "rss.db", iosDatabaseLocation:'default'});
//         //     //var dbm = window.sqlitePlugin.openDatabase({ name: "rss.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1 });
//         // } else{
//         //     // Works on iOS
//         //     var dbm = window.sqlitePlugin.openDatabase({ name: "rss.db", location: 2, createFromLocation: 1});
//         // }
//         //var dbm = window.sqlitePlugin.openDatabase({ name: "rss.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1 });
//         var dbm = $cordovaSQLite.openDB({ name: "rss.db", iosDatabaseLocation:'default'});
//         $scope.entries = data.rss.channel.item;
//         window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
//         for (var i = 0; i < data.rss.channel.item.length; i++) {
//             $scope.progressing++;
//             getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
//                 var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";
//
//                 dbm.transaction(function (tx) {
//                         tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {
//
//                         }, function (error) {
//
//                         })
//                     },
//                     function (error) {
//
//                     }, function (resp) {
//
//                     });
//
//             })
//         }
//         $ionicLoading.hide();
//         $scope.$broadcast('scroll.refreshComplete');
//     })
//     .error(function (error) {
//         $ionicLoading.hide();
//         //If it fails to load: showing Connect Internet alert
//         $scope.server_fail = true;
//         $scope.$broadcast('scroll.refreshComplete');
//     });
                }
            })
        };

        //$scope.checkDiff = checkDates();
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.doRefresh = function () {
            $scope.refreshed = false;
            init();
        };
        $scope.showMaterial = function (type, url, id, header) {
            if (type == 'news') {
                header = settings.HEADERS.news;
            }
            $state.go("app.getMaterial", {type: type, urlMaterial: url, id: id, header: header});
        };
        $scope.LoadMore = function () {
            console.log('getLentaMainCtrl :: LoadMore : $state.params=', $state.params);
            init();
            $state.params.newspage = parseInt($state.params.newspage) + 1;
        };

        getDataFromBD(DATA.alias, '*');
    })

    .controller('getLentaArticlesCtrl', function ($http, $scope, $state, $q, settings, $cordovaNetwork, $cordovaSQLite, $ionicPlatform, $localstorage, DATA, $ionicLoading, $ionicScrollDelegate) {
        console.log('getLentaArticlesCtrl :: begin : $state=', $state);

        $scope.API = DATA;
        $scope.LANG = settings.LANG;
        $scope.refreshed = true;
        $scope.server_fail = false;
        $scope.internet_fail = false;
        $scope.progressing = 0;
        $scope.LABELS = settings.LABELS;
        $scope.header = settings.HEADERS.prefix + $state.params.header;

        var name = "entries" + $scope.API.alias;
        var checkOffline = function () {
            return $cordovaNetwork.isOffline();
        };
        var init = function () {
            $ionicPlatform.ready(function () {
                var aid = $scope.API.alias;
                $scope.internet_fail = checkOffline();
                if (window.localStorage['FirstRun'] !== undefined) {
                    if (JSON.parse(window.localStorage['FirstRun']) == true && $scope.internet_fail == false) {
                        $scope.refreshed = false;
                        window.localStorage['FirstRun'] = false;
                    }
                }
                if (window.localStorage[name] !== undefined && $scope.refreshed == true) {
                    $scope.server_fail = false;
                    $scope.entries = JSON.parse(window.localStorage[name]);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                else {
                    $scope.refreshed = true;
                    if ($scope.articles == undefined) {
                        $scope.articles = [];
                    }
                    $http.get($scope.API.link, {
                        params: {
                            "page": $state.params.page,
                            "category": $state.params.category
                        }
                    })
                        .success(function (data) {
                            console.log('getLentaArticlesCtrl :: init : $http.get=', data);
                            console.log('getLentaArticlesCtrl :: init : $state.params=', $state.params);

                            $scope.server_fail = false;
                            window.localStorage['FirstRUn'] = 'false';
                            $scope.articles.push(data);

                            var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default'});
                            // $scope.entries = data.rss.channel.item;
                            // window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
                            // for (var i = 0; i < data.rss.channel.item.length; i++) {
                            //     $scope.progressing++;
                            //     getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
                            //         var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";
                            //
                            //         dbm.transaction(function (tx) {
                            //                 tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {
                            //
                            //                 }, function (error) {
                            //
                            //                 })
                            //             },
                            //             function (error) {
                            //
                            //             }, function (resp) {
                            //
                            //             });
                            //
                            //     })
                            // }
                            $ionicLoading.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        })
                        .error(function (error) {
                            $ionicLoading.hide();
                            //If it fails to load: showing Connect Internet alert
                            $scope.server_fail = true;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                }
            })
        };

        //$scope.checkDiff = checkDates();
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.doRefresh = function () {
            $scope.refreshed = false;
            init();
        };
        $scope.showMaterial = function (type, url, id) {
            $state.go("app.getMaterial", {
                type: type,
                urlMaterial: url,
                id: id,
                header: $state.params.header,
                label: $state.params.label
            });
        };
        $scope.LoadMore = function (page) {
            if ($state.params.page > 0) {
                init();
            }
            $state.params.page = $state.params.page + 1;
        };

        init();
    })

    .controller('getLentaArticlesByAuthorCtrl', function ($http, $scope, $state, $q, settings, $cordovaNetwork, $cordovaSQLite, $ionicPlatform, $localstorage, DATA, $ionicLoading, $ionicScrollDelegate) {
        console.log('getLentaArticlesByAuthorCtrl :: begin : $state=', $state);

        $scope.API = DATA;
        $scope.LANG = settings.LANG;
        $scope.refreshed = true;
        $scope.server_fail = false;
        $scope.internet_fail = false;
        $scope.progressing = 0;
        $scope.LABELS = settings.LABELS;
        $scope.header = settings.HEADERS.prefix + settings.HEADERS.author;
        $scope.end = 1;

        var name = "entries" + $scope.API.alias;
        var checkOffline = function () {
            return $cordovaNetwork.isOffline();
        };
        var init = function () {
            $ionicPlatform.ready(function () {
                var aid = $scope.API.alias;
                $scope.internet_fail = checkOffline();
                if (window.localStorage['FirstRun'] !== undefined) {
                    if (JSON.parse(window.localStorage['FirstRun']) == true && $scope.internet_fail == false) {
                        $scope.refreshed = false;
                        window.localStorage['FirstRun'] = false;
                    }
                }
                if (window.localStorage[name] !== undefined && $scope.refreshed == true) {
                    $scope.server_fail = false;
                    $scope.entries = JSON.parse(window.localStorage[name]);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                else {
                    $scope.refreshed = true;
                    if ($scope.articles == undefined) {
                        $scope.articles = [];
                    }
                    $http.get($scope.API.link, {
                        params: {
                            "page": $state.params.page,
                            "id": $state.params.id
                        }
                    })
                        .success(function (data) {
                            console.log('getLentaArticlesByAuthorCtrl :: init : $http.get=', data);
                            console.log('getLentaArticlesByAuthorCtrl :: init : $state.params=', $state.params);

                            $scope.server_fail = false;
                            window.localStorage['FirstRUn'] = 'false';
                            $scope.author = data.author;
                            $scope.end = data.end;
                            $scope.articles.push(data.items);

                            var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default'});
                            // $scope.entries = data.rss.channel.item;
                            // window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
                            // for (var i = 0; i < data.rss.channel.item.length; i++) {
                            //     $scope.progressing++;
                            //     getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
                            //         var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";
                            //
                            //         dbm.transaction(function (tx) {
                            //                 tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {
                            //
                            //                 }, function (error) {
                            //
                            //                 })
                            //             },
                            //             function (error) {
                            //
                            //             }, function (resp) {
                            //
                            //             });
                            //
                            //     })
                            // }
                            $ionicLoading.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        })
                        .error(function (error) {
                            $ionicLoading.hide();
                            //If it fails to load: showing Connect Internet alert
                            $scope.server_fail = true;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                }
            })
        };

        //$scope.checkDiff = checkDates();
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.doRefresh = function () {
            $scope.refreshed = false;
            init();
        };
        $scope.showMaterial = function (type, url, id, category) {
            $state.go("app.getMaterial", {
                type: type,
                urlMaterial: url,
                id: id,
                // header: $state.params.header,
                header: category,
                label: $state.params.label
            });
        };
        $scope.LoadMore = function (page) {
            if ($state.params.page > 0) {
                init();
            }
            $state.params.page = $state.params.page + 1;
        };

        init();
    })

    .controller('getLentaNewsCtrl', function ($http, $scope, $state, $q, settings, $cordovaNetwork, $cordovaSQLite, $ionicPlatform, $localstorage, DATA, $ionicLoading) {
        console.log('getLentaNewsCtrl :: begin : $state=', $state);

        $scope.API = DATA;
        $scope.LANG = settings.LANG;
        $scope.refreshed = true;
        $scope.server_fail = false;
        $scope.internet_fail = false;
        $scope.progressing = 0;
        $scope.header = settings.HEADERS.prefix + settings.HEADERS.news;

        var name = "entries" + DATA.alias;
        var checkOffline = function () {
            return $cordovaNetwork.isOffline();
        };
        var init = function () {
            $ionicPlatform.ready(function () {
                var aid = DATA.id;
                $scope.internet_fail = checkOffline();
                if (window.localStorage['FirstRun'] !== undefined) {
                    if (JSON.parse(window.localStorage['FirstRun']) == true && $scope.internet_fail == false) {
                        $scope.refreshed = false;
                        window.localStorage['FirstRun'] = false;
                    }
                }
                if (window.localStorage[name] !== undefined && $scope.refreshed == true) {
                    $scope.server_fail = false;
                    $scope.entries = JSON.parse(window.localStorage[name]);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                else {
                    $scope.refreshed = true;
                    if ($scope.news == undefined) {
                        $scope.news = [];
                        $state.params.page = 0;
                    }

                    //if not adding the rss to locale storage and adding the news to the database through http request
                    $http.get($scope.API.link, {params: {"page": $state.params.page}})
                        .success(function (data) {
                            console.log('getLentaNewsCtrl :: init : $http.get=', data);

                            $scope.server_fail = false;
                            window.localStorage['FirstRUn'] = 'false';
                            //$scope.news = data;
                            $scope.news.push(data);

                            var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default'});
                            // $scope.entries = data.rss.channel.item;
                            // window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
                            // for (var i = 0; i < data.rss.channel.item.length; i++) {
                            //     $scope.progressing++;
                            //     getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
                            //         var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";
                            //
                            //         dbm.transaction(function (tx) {
                            //                 tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {
                            //
                            //                 }, function (error) {
                            //
                            //                 })
                            //             },
                            //             function (error) {
                            //
                            //             }, function (resp) {
                            //
                            //             });
                            //
                            //     })
                            // }
                            $ionicLoading.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        })
                        .error(function (error) {
                            $ionicLoading.hide();
                            //If it fails to load: showing Connect Internet alert
                            $scope.server_fail = true;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                }
            })
        };

        //$scope.checkDiff = checkDates();
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.doRefresh = function () {
            $scope.refreshed = false;
            init();
        };
        $scope.showMaterial = function (type, url, id) {
            $state.go("app.getMaterial", {type: type, urlMaterial: url, id: id});
        };
        $scope.LoadMore = function (page) {
            $scope.loading = true;
            if ($state.params.page > 0) {
                init();
            }
            $state.params.page = $state.params.page + 1;
        };

        init();
    })

    .controller('getLentaGalleriesCtrl', function ($http, $scope, $state, $q, settings, $cordovaNetwork, $cordovaSQLite, $ionicPlatform, $localstorage, DATA, $ionicLoading, $ionicScrollDelegate) {
        console.log('getLentaGalleriesCtrl :: begin : $state=', $state);

        $scope.refreshed = true;
        $scope.server_fail = false;
        $scope.internet_fail = false;
        $scope.progressing = 0;
        $scope.LANG = settings.LANG;
        $scope.header = settings.HEADERS.prefix + $state.params.header;

        var name = "entries" + DATA.alias;
        var checkOffline = function () {
            return $cordovaNetwork.isOffline();
        };
        var init = function () {
            $ionicPlatform.ready(function () {
                var aid = DATA.alias;
                $scope.internet_fail = checkOffline();
                if (window.localStorage['FirstRun'] !== undefined) {
                    if (JSON.parse(window.localStorage['FirstRun']) == true && $scope.internet_fail == false) {
                        $scope.refreshed = false;
                        window.localStorage['FirstRun'] = false;
                    }
                }
                if (window.localStorage[name] !== undefined && $scope.refreshed == true) {
                    $scope.server_fail = false;
                    $scope.entries = JSON.parse(window.localStorage[name]);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                else {
                    $scope.refreshed = true;
                    if ($scope.galleries == undefined) {
                        $scope.galleries = [];
                    }
                    $http.get(DATA.link, {params: {"page": $state.params.page, "category": $state.params.category}})
                        .success(function (data) {
                            console.log('getLentaGalleriesCtrl :: init : $http.get=', data);
                            console.log('getLentaGalleriesCtrl :: init : $state.params=', $state.params);

                            $scope.server_fail = false;
                            window.localStorage['FirstRUn'] = 'false';
                            $scope.galleries.push(data);

                            var dbm = $cordovaSQLite.openDB({name: settings.dbname, iosDatabaseLocation: 'default'});
                            // $scope.entries = data.rss.channel.item;
                            // window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
                            // for (var i = 0; i < data.rss.channel.item.length; i++) {
                            //     $scope.progressing++;
                            //     getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
                            //         var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";
                            //
                            //         dbm.transaction(function (tx) {
                            //                 tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {
                            //
                            //                 }, function (error) {
                            //
                            //                 })
                            //             },
                            //             function (error) {
                            //
                            //             }, function (resp) {
                            //
                            //             });
                            //
                            //     })
                            // }
                            $ionicLoading.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        })
                        .error(function (error) {
                            $ionicLoading.hide();
                            //If it fails to load: showing Connect Internet alert
                            $scope.server_fail = true;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                }
            })
        };

        //$scope.checkDiff = checkDates();
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.doRefresh = function () {
            $scope.refreshed = false;
            init();
        };
        $scope.showMaterial = function (type, url, id) {
            $state.go("app.getMaterial", {type: type, urlMaterial: url, id: id, header: $state.params.header});
        };
        $scope.LoadMore = function (page) {
            if ($state.params.page > 0) {
                init();
            }
            $state.params.page = $state.params.page + 1;
        };

        init();
    })

    .controller('getMaterialCtrl', function ($scope, $state, $stateParams, $http, $q, DATA, settings, FavoriteService, $sce, $cordovaSQLite, $ionicPlatform, $localstorage, $cordovaNetwork, $cordovaSocialSharing, $ionicSlideBoxDelegate, $filter) {
        console.log('getMaterialCtrl :: begin : $state=', $state);

        $scope.isOffline = $cordovaNetwork.isOffline();
        $scope.fail = false;
        $scope.title = '';
        $scope.LANG = settings.LANG;
        $scope.header = settings.HEADERS.prefix;

        var getMaterial = function (type, id, url, title, desc) {
            var result = {
                linc: url,
                data: null,
                title: title,
                desc: desc
            };
            var defer = $q.defer();
            $http.get(DATA.link, {params: {"id": id}}).then(function (response) {
                result.data = response.data;
                console.log('getMaterialCtrl :: getMaterial : $http.get=', result);
                $scope.progressing--;
                defer.resolve(result);
            }, function (response) {
                defer.reject(response);
            });
            return defer.promise;
        };

        if (window.localStorage[name] !== undefined) {
            temp = JSON.parse(window.localStorage[name]);
            for (var i = 0; i < temp.length; i++) {
                if (temp[i].link == url)
                    temp[i].read = 'true';
                window.localStorage[name] = JSON.stringify(temp);
            }
        }

        switch ($stateParams.type) {
            case 'article':
                getMaterial($stateParams.type, $stateParams.id, $stateParams.urlMaterial, "default_title", "default_description").then(function (response) {
                    $scope.url = response.data.url;
                    $scope.id = response.data.id;
                    $scope.model = response.data.model;
                    $scope.title = response.data.title;
                    $scope.title2 = response.data.title2;
                    $scope.date = response.data.date;
                    $scope.authors = response.data.authors;
                    $scope.author = response.data.author;
                    $scope.author_id = response.data.author_ids;
                    $scope.img = response.data.img;
                    $scope.img_name = response.data.img_name;
                    $scope.img_author = response.data.img_author;
                    $scope.parts = response.data.parts;
                    $scope.readmore = response.data.readmore;
                    $scope.category = response.data.category;
                    $scope.label = response.data.label;

                    if ($stateParams.header == ':header') {
                        $scope.header = settings.HEADERS.prefix + $scope.category;
                    } else {
                        $scope.header = settings.HEADERS.prefix + $stateParams.header;
                    }
                    FavoriteService.material = {
                        id: $scope.id,
                        model: $scope.model,
                        title: $scope.title,
                        img: $scope.img,
                        read: 0,
                        favorite: 0
                    };
                    FavoriteService.getFavoriteStatus($scope.model, $scope.id, $scope.title);

                    $scope.sliderOptions = {
                        initialSlide: 1,
                        direction: 'horizontal',
                        loop: false,
                        effect: 'fade',
                        pager: true,
                        pagination: true,
                        showPager: true,
                        speed: 500
                    };
                    $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                        console.log('data=', data);
                        // data.slider is the instance of Swiper
                        $scope.slider = data.slider;
                        $ionicSlideBoxDelegate.update();
                    });
                    $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
                        console.log('Slide change is beginning');
                    });
                    $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                        console.log('data=', data);
                        // note: the indexes are 0-based
                        $scope.activeIndex = data.slider.activeIndex;
                        $scope.previousIndex = data.slider.previousIndex;
                    });
                    $ionicSlideBoxDelegate.update();
                });
                break;

            case 'news':
                $scope.header = settings.HEADERS.prefix + settings.HEADERS.news;
                getMaterial($stateParams.type, $stateParams.id, $stateParams.urlMaterial, "default_title", "default_description").then(function (response) {
                    $scope.id = response.data.id;
                    $scope.model = 'news';
                    $scope.url = response.data.url;
                    $scope.title = response.data.title;
                    $scope.title2 = response.data.title2;
                    $scope.date = response.data.date;
                    $scope.author = response.data.author;
                    $scope.img = response.data.img;
                    $scope.img_name = response.data.img_name;
                    $scope.img_author = response.data.img_author;
                    $scope.parts = response.data.parts;
                    $scope.readmore = response.data.readmore;

                    FavoriteService.material = {
                        id: $scope.id,
                        model: $scope.model,
                        title: $scope.title,
                        img: $scope.img,
                        read: 0,
                        favorite: 0
                    };
                    FavoriteService.getFavoriteStatus($scope.model, $scope.id, $scope.title);

                    $scope.sliderOptions = {
                        initialSlide: 1,
                        direction: 'horizontal',
                        loop: false,
                        effect: 'fade',
                        pager: true,
                        pagination: true,
                        showPager: true,
                        speed: 500
                    };
                    $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                        console.log('data=', data);
                        // data.slider is the instance of Swiper
                        $scope.slider = data.slider;
                        $ionicSlideBoxDelegate.update();
                    });
                    $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
                        console.log('Slide change is beginning');
                    });
                    $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                        console.log('data=', data);
                        // note: the indexes are 0-based
                        $scope.activeIndex = data.slider.activeIndex;
                        $scope.previousIndex = data.slider.previousIndex;
                    });
                    $ionicSlideBoxDelegate.update();
                });
                break;

            case 'gallery':
                $scope.header = settings.HEADERS.prefix + settings.HEADERS.gallery;
                getMaterial($stateParams.type, $stateParams.id, $stateParams.urlMaterial, "default_title", "default_description").then(function (response) {
                    //$scope.url = response.data.url;
                    $scope.id = response.data.id;
                    $scope.photos = response.data.image;
                    $scope.model = response.data.model;
                    $scope.title = response.data.title;
                    $scope.date = response.data.date;
                    $scope.description = response.data.description;
                    $scope.img = response.data.img;
                    $scope.imgPath = response.data.imgpath;
                    $scope.readmore = response.data.readmore;
                    $scope.label = response.data.label;

                    FavoriteService.material = {
                        id: $scope.id,
                        model: $scope.model,
                        title: $scope.title,
                        img: $scope.img.thumbImg,
                        read: 0,
                        favorite: 0
                    };
                    FavoriteService.getFavoriteStatus($scope.model, $scope.id, $scope.title);

                    $scope.sliderOptions = {
                        initialSlide: 1,
                        direction: 'horizontal',
                        loop: false,
                        effect: 'fade',
                        pager: true,
                        pagination: true,
                        showPager: true,
                        speed: 500
                    };
                    $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
                        console.log('data=', data);
                        // data.slider is the instance of Swiper
                        $scope.slider = data.slider;
                        $ionicSlideBoxDelegate.update();
                    });
                    $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
                        console.log('Slide change is beginning');
                    });
                    $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
                        console.log('data=', data);
                        // note: the indexes are 0-based
                        $scope.activeIndex = data.slider.activeIndex;
                        $scope.previousIndex = data.slider.previousIndex;
                    });
                    $ionicSlideBoxDelegate.update();
                });
                break;

            default:
                break;
        }

        $scope.doRefresh = function () {
            $state.go($state.current, {}, {reload: true});
            $scope.$broadcast('scroll.refreshComplete');
        };
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.fromJson = function (json) {
            return JSON.parse(json);
        };
        $scope.showMaterial = function (model, url, id, header) {
            $state.go("app.getMaterial", {type: model, urlMaterial: url, id: id, header: header});
        };
        $scope.showAuthor = function (model, id, author) {
            FavoriteService.prepForBroadcast(0);
            $state.go("app.getArticlesByAuthor", {model: model, id: id, author: author, page:0});
        };
    })

    .filter('showImage', function () {
        return function (label) {
            var array = ['lime', 'tv'];
            return array.indexOf(label) == -1;
        };
    })

    .filter('showLabel', function (settings) {
        return function (label) {
            var array = [settings.LABELS.lime, settings.LABELS.tv, settings.LABELS.interview, settings.LABELS.opinion, settings.LABELS.gallery];
            return array.indexOf(label) >= 0;
        };
    })

    .filter('showLabelModel', function (settings) {
        return function (model) {
            return settings.LABELS[model];
        };
    })

    .filter('from_json', function (json) {
        return JSON.parse(json);
    })

    .filter('capitalize', function () {
        return function (input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    })

    .directive('imageGallery', function () {
        return {
            restrict: 'E',
            scope: true, //чтобы не засорять родительский скоп
            link: function (scope, element, attrs) {
                scope.images = JSON.parse(attrs["option"]);
            },
            templateUrl: 'templates/gallery.html'
        }
    })

    .directive('imageShare', function () {
        return {
            restrict: 'E',
            scope: true, //чтобы не засорять родительский скоп
            link: function (scope, element, attrs) {
                scope.imgUrl = attrs["option"];
            },
            templateUrl: 'templates/imgShare.html'
        }
    })

    .directive('readMore', function () {
        return {
            restrict: 'E',
            scope: true,
            link: function (scope, element, attrs) {
                scope.type = attrs["type"];
                scope.materials = JSON.parse(attrs["option"]);
            },
            templateUrl: 'templates/readmore.html'
        }
    })

    .directive('shareMaterial', function () {
        return {
            restrict: 'E',
            scope: true,
            link: function (scope, element, attrs) {
                scope.url = attrs["url"];
            },
            templateUrl: 'templates/share_material.html'
        }
    })

    .controller('sliderCtrl', function ($scope, $ionicSlideBoxDelegate) {
        $scope.navSlide = function (index) {
            $ionicSlideBoxDelegate.slide(index, 500);
            $ionicSlideBoxDelegate.update();
        }
    })

    .controller('shareCtrl', function ($scope, settings, $cordovaSocialSharing, $ionicPopup) {
        $scope.ShareImageFacebook = function (imgUrl) {
            $cordovaSocialSharing.shareViaFacebook(settings.LANG.title, imgUrl, null)
                .then(function (result) {
                    console.log('shareCtrl :: ShareFacebook : Success=', result);
                    /*var message = 'Спасибо за то, что поделились';
                     if(!result){
                     message = 'Please share us';
                     }
                     var alertPopup = $ionicPopup.alert({
                     title: message
                     });
                     alertPopup.then(function(res) {
                     });*/
                }).catch(function (result) {
                    console.log('shareCtrl :: ShareFacebook : error=', result);
                var alertPopup = $ionicPopup.alert({
                    title: 'Невозможно поделиться через Facebook',
                    template: 'Возможно у вас не инсталлировано Facebook-приложение'
                });
                alertPopup.then(function (res) {
                });
            });
        };
        $scope.ShareFacebook = function (url) {
            //console.log('ShareFacebook :: $scope.url=', $scope.url);
            // $cordovaSocialSharing.shareViaFacebook('message', $scope.img, $scope.url)
            $cordovaSocialSharing.shareViaFacebook(settings.LANG.title, null, $scope.url)
                .then(function (result) {
                    console.log('shareCtrl :: ShareFacebook : Success=', result);
                    /*var message = 'Спасибо за то, что поделились';
                     if(!result){
                     message = 'Please share us';
                     }
                     var alertPopup = $ionicPopup.alert({
                     title: message
                     });
                     alertPopup.then(function(res) {
                     });*/
                }).catch(function (result) {
                console.log('shareCtrl :: ShareFacebook : error=', result);
                var alertPopup = $ionicPopup.alert({
                    title: 'Невозможно поделиться через Facebook',
                    template: 'Возможно у вас не инсталлировано Facebook-приложение'
                });
                alertPopup.then(function (res) {
                });
            });
        };
        $scope.ShareVkontakte = function (url) {
            $cordovaSocialSharing.share(settings.LANG.title, $scope.img, url)
            //$cordovaSocialSharing.shareVia('com.android.vkontakte', 'Message via Vkontakte', null, null, null)
                .then(function (result) {
                    console.log('shareCtrl :: ShareVkontakte : Success=', result);
                }).catch(function (result) {
                console.log('shareCtrl :: ShareVkontakte : error=', result);
            });
        };
        $scope.ShareTwitter = function (url) {
            $cordovaSocialSharing.shareViaTwitter(settings.LANG.title, $scope.img, url)
                .then(function (result) {
                    console.log('shareCtrl :: ShareTwitter : Success=', result);
                }).catch(function (result) {
                console.log('shareCtrl :: ShareTwitter : error=', result);
                var alertPopup = $ionicPopup.alert({
                    title: 'Невозможно поделиться через Twitter',
                    template: 'Возможно у вас не инсталлировано Twitter-приложение'
                });
                alertPopup.then(function (res) {
                });
            });
        };
        $scope.ShareImageTwitter = function (imgUrl) {
            $cordovaSocialSharing.shareViaTwitter(settings.LANG.title, imgUrl, null)
                .then(function (result) {
                    console.log('shareCtrl :: ShareTwitter : Success=', result);
                }).catch(function (result) {
                console.log('shareCtrl :: ShareTwitter : error=', result);
                var alertPopup = $ionicPopup.alert({
                    title: 'Невозможно поделиться через Twitter',
                    template: 'Возможно у вас не инсталлировано Twitter-приложение'
                });
                alertPopup.then(function (res) {
                });
            });
        };
    })

    .controller('favoritesCtrl', function ($scope, settings, FavoriteService, $cordovaSQLite, $ionicPlatform, $state, $localstorage) {
        console.log('favoritesCtrl :: $state=',$state);

        FavoriteService.prepForBroadcast(0);

        $scope.header = settings.HEADERS.prefix + settings.HEADERS.favorites;

        $scope.entries = [];
        $scope.init = function () {
            var query = ('SELECT * FROM materials WHERE fav=?;');
            $ionicPlatform.ready(function () {
                $cordovaSQLite.execute(db, query, [1]).then(function (res) {
                    var len = res.rows.length;
                    // console.log('len=',len);
                    if (len > 0) {
                        for (j = 0; j < len; j++) {
                            // console.log('res.rows.item(j)=',res.rows.item(j));
                            $scope.entries.push(res.rows.item(j));
                            //console.log('$scope.entries=',$scope.entries);
                            $scope.noresult = false;
                        }
                    }
                    else {
                        $scope.noresult = true;
                    }
                }, function (err) {

                })
            })
        };
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.showArticle = function (v) {
            $state.go("app.articleF", {urlArticle: v});
        };
        $scope.showMaterial = function (model, url, id) {
            $state.go("app.getMaterial", {type: model, urlMaterial: url, id: id, header: ''});
        };
    })



    .controller('NewCtrl', function ($http, $scope, $state, $q, $cordovaNetwork, $cordovaSQLite, $ionicPlatform, $localstorage, RSS, $ionicLoading) {

        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        $ionicLoading.hide();
        //Getting the RSS object from the menu
        $scope.RSS = RSS;
        $scope.refreshed = true;
        $scope.server_fail = false;
        $scope.internet_fail = false;
        var checkOffline = function () {
            return $cordovaNetwork.isOffline();
        };
        //console.log('$scope=',$scope);
        var name = "entries" + $scope.RSS.id;
        //dl article countre
        $scope.progressing = 0;
        //Setting the color schema
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        //Setting the font size
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };

        //Function for asynchronious http request
        var getArticle = function (url, title, desc) {
            var result =
            {
                linc: url,
                json: null,
                title: title,
                desc: desc
            }
            var defer = $q.defer();
            $http.get("http://apostrophe.com.ua/api/parser/articleParse.php", {params: {"q": url}}).then(function (response) {
                result.json = response;
                $scope.progressing--;
                defer.resolve(result);
            }, function (response) {
                defer.reject(response);
            });
            return defer.promise;
        };
        //Function running on refreshing
        $scope.doRefresh = function () {
            $scope.refreshed = false;
            init();
        };
        //$scope.checkDiff = checkDates();

        var init = function () {
            $ionicPlatform.ready(function () {
                var aid = $scope.RSS.id;
                $scope.internet_fail = checkOffline();
                if (window.localStorage['FirstRun'] !== undefined) {
                    if (JSON.parse(window.localStorage['FirstRun']) == true && $scope.internet_fail == false) {
                        $scope.refreshed = false;
                        window.localStorage['FirstRun'] = false;
                    }
                }
                if (window.localStorage[name] !== undefined && $scope.refreshed == true) {
                    $scope.server_fail = false;
                    $scope.entries = JSON.parse(window.localStorage[name]);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                else {
                    $scope.refreshed = true;
                    //if not adding the rss to locale storage and adding the news to the database through http request
                    $http.get("http://apostrophe.com.ua/api/parser.php", {params: {"rss_url": $scope.RSS.link}})
                        .success(function (data) {
                            $scope.server_fail = false;
                            window.localStorage['FirstRUn'] = 'false';
                            for (var j = 0; j < data.rss.channel.item.length; j++) {
                                data.rss.channel.item[j].read = 'false';
                                data.rss.channel.item[j].theDate = Date.parse(data.rss.channel.item[j].pubDate);
                            }
                            ;
                            // console.log('$cordovaDevice.getPlatform()='+$cordovaDevice.getPlatform());
                            // if($cordovaDevice.getPlatform()=="Android")﻿{
                            //     // Works on android but not in iOS
                            //     var dbm = $cordovaSQLite.openDB({ name: "rss.db", iosDatabaseLocation:'default'});
                            //     //var dbm = window.sqlitePlugin.openDatabase({ name: "rss.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1 });
                            // } else{
                            //     // Works on iOS
                            //     var dbm = window.sqlitePlugin.openDatabase({ name: "rss.db", location: 2, createFromLocation: 1});
                            // }
                            //var dbm = window.sqlitePlugin.openDatabase({ name: "rss.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1 });
                            var dbm = $cordovaSQLite.openDB({name: "rss.db", iosDatabaseLocation: 'default'});
                            $scope.entries = data.rss.channel.item;
                            window.localStorage[name] = JSON.stringify(data.rss.channel.item);//initial backup
                            for (var i = 0; i < data.rss.channel.item.length; i++) {
                                $scope.progressing++;
                                getArticle(data.rss.channel.item[i].link, data.rss.channel.item[i].title, data.rss.channel.item[i].description).then(function (resp) {
                                    console.log('resp', resp);
                                    var query = "insert into article(link_id, feed_id, json_article, fav,title, description) values (?,?,?,?,?,?);";

                                    dbm.transaction(function (tx) {
                                            tx.executeSql(query, [resp.linc, aid, JSON.stringify(resp.json.data), false, resp.title, resp.desc], function () {

                                            }, function (error) {

                                            })
                                        },
                                        function (error) {

                                        }, function (resp) {

                                        });

                                })
                            }
                            $ionicLoading.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                        })
                        .error(function (error) {
                            $ionicLoading.hide();
                            //If it fails to load: showing Connect Internet alert
                            $scope.server_fail = true;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                }
            })
        }

        //Shows chosen article in new window
        $scope.showArticle = function (url) {
            console.log('New :: showArticle : url=', url);
            if (window.localStorage[name] !== undefined) {
                temp = JSON.parse(window.localStorage[name]);
                console.log('New :: showArticle : temp=', temp)
                for (var i = 0; i < temp.length; i++) {
                    if (temp[i].link == url)
                        temp[i].read = 'true';
                    window.localStorage[name] = JSON.stringify(temp);
                }
                ;
            }
            $state.go("app.article", {urlArticle: url});
        }

        //Shows chosen material in new window
        $scope.showMaterial = function (type, url, id) {
            console.log('showArticle :: url=', url);
            console.log('showArticle :: id=', id);
            if (window.localStorage[name] !== undefined) {
                temp = JSON.parse(window.localStorage[name]);
                for (var i = 0; i < temp.length; i++) {
                    if (temp[i].link == url)
                        temp[i].read = 'true';
                    window.localStorage[name] = JSON.stringify(temp);
                }
                ;
            }
            // if (type == 'article') {
            $state.go("app.getArticle", {type: type, urlArticle: url, id: id});
            // }
            // if (type == 'news') {
            //     $state.go("app.getNews", {urlArticle: url, id: id});
            // }
        }

        //Call init function
        init();

        //Call init function
        init();
    })

    .controller('parametreCtrl', function ($scope, $state, $localstorage, $cordovaSQLite, $ionicHistory) {

        $scope.textsize = {value: ($localstorage.get('TextSize'))};
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        }
        var bbool = function (key) {
            if ($localstorage.get(key) === "true")
                return true;
            else
                return false;
        };

        $scope.SetList = [{name: "Темная цветовая схема", value: bbool("DarkMode")}];
        $scope.CCache = {name: "Почистить базу данных"};
        $scope.Apply = {name: "Принять и сохранить"};

        $scope.ccache = function () {
            var query = "DELETE FROM article; VACUUM;";
            $cordovaSQLite.execute(db, query).then(function () {

            }, function (err) {

            });

            window.localStorage.clear();
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $scope.apply();
        };

        $scope.apply = function () {
            $localstorage.set('DarkMode', $scope.SetList[0].value);
            $localstorage.set('TextSize', $scope.textsize.value);
        };

    })

    .controller('articleCtrl', function ($scope, $state, $stateParams, $http, $cordovaSQLite, $ionicPlatform, $localstorage, $cordovaNetwork) {

        $scope.isOffline = $cordovaNetwork.isOffline();
        $scope.doRefresh = function () {
            console.log('$state.current=', $state.current);
            $state.go($state.current, {}, {reload: true});
            $scope.$broadcast('scroll.refreshComplete');
        };
        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        };
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.fail = false;

        $ionicPlatform.ready(function () {
            var ar;
            var query = "SELECT json_article FROM article WHERE link_id=?";
            var db = $cordovaSQLite.openDB({name: "rss.db", iosDatabaseLocation: 'default'});
            $cordovaSQLite.execute(db, query, [$stateParams.urlArticle]).then(function (res) {
                console.log('res=', res);
                if (res.rows.length > 0) {
                    $scope.fail = false;

                    ar = JSON.parse(res.rows.item(0).json_article);
                    $scope.text = ar.article;
                    $scope.titleA = ar.title;
                    $scope.dateA = ar.date;
                    $scope.authorA = ar.author;
                } else {
                    $scope.fail = true;
                }
            }, function (err) {
                console.log('articleCtrl :: err=', err);
            });
            var bbool = function (input) {
                if (input === "true")
                    return true
                else
                    return false
            };

            var query = "SELECT fav FROM article WHERE link_id=?";
            $cordovaSQLite.execute(db, query, [$stateParams.urlArticle]).then(function (res) {
                if (res.rows.length > 0) {

                    $scope.alreadyFav = bbool(res.rows.item(0).fav);
                } else {

                }
            }, function (err) {

            });

        });

        $scope.fav = function (v) {
            $ionicPlatform.ready(function () {
                var query = ('UPDATE article set fav=? where link_id=?;');
                $cordovaSQLite.execute(db, query, [v, $stateParams.urlArticle]).then(function (res) {
                    $scope.alreadyFav = v;

                }, function (err) {

                })
            })
        }
    })

    .controller('favorites_oldCtrl', function ($scope, $cordovaSQLite, $ionicPlatform, $state, $localstorage) {

        $scope.checkdark = function () {
            return $localstorage.get('DarkMode');
        }
        $scope.checkblind = function () {
            return $localstorage.get('TextSize');
        };
        $scope.entries = [];
        $scope.init = function () {
            var query = ('SELECT title,description,link_id FROM article WHERE fav=?;');
            $ionicPlatform.ready(function () {
                $cordovaSQLite.execute(db, query, [true]).then(function (res) {
                    var len = res.rows.length;
                    if (len > 0) {
                        for (j = 0; j < len; j++) {

                            $scope.entries.push(res.rows.item(j));
                            $scope.noresult = false;
                        }
                    }
                    else {

                        $scope.noresult = true;
                    }
                }, function (err) {

                })
            })
        }
        $scope.showArticle = function (v) {
            $state.go("app.articleF", {urlArticle: v});
        }
    })

;


