angular.module('starter.services', [])

    .service('TypeService', ['$q', 'settings', function ($q, settings) {
        return {
            DATA: [
                {
                    type: 'article',
                    //title: 'A: Статья',
                    link: settings.HOST + settings.API.getMaterialArticle,
                    image: true
                },
                {
                    type: 'news',
                    title: 'A: Новость',
                    link: settings.HOST + settings.API.getMaterialNews,
                    image: true
                },
                {
                    type: 'gallery',
                    title: 'Фотовзгляд',
                    link: settings.HOST + settings.API.getMaterialGallery,
                    image: true
                }
            ],
            getDATA: function () {
                return this.DATA
            },
            getDATA: function (TYPE) {
                var dfd = $q.defer();
                this.DATA.forEach(function (DATA) {
                    if (DATA.type === TYPE) dfd.resolve(DATA)
                });
                return dfd.promise
            }
        }
    }])
    
    .service('AliasService', ['$q', 'settings', function ($q, settings) {
        return {
            DATA: [
                {
                    alias: 'Main',
                    link: settings.HOST + settings.API.getMain,
                    title: 'Главная',
                    image: true
                },
                {
                    alias: 'Articles',
                    link: settings.HOST + settings.API.getArticles,
                    title: 'Статьи',
                    // titles: {'interviews':'Интервью', 'opinions':'Мнение', 'tv':'Апостроф TV'},
                    image: true
                },
                {
                    alias: 'ArticlesByAuthor',
                    link: settings.HOST + settings.API.getArticlesByAuthor,
                    title: 'Статьи автора',
                    image: true
                },
                {
                    alias: 'News',
                    link: settings.HOST + settings.API.getNews,
                    title: 'Новости',
                    image: true
                },
                {
                    alias: 'Galleries',
                    link: settings.HOST + settings.API.getGalleries,
                    title: 'Фотовзгляд',
                    image: true
                }
            ],
            getDATA: function () {
                return this.DATA
            },
            getDATA: function (ID) {
                var dfd = $q.defer();
                this.DATA.forEach(function (DATA) {
                    if (DATA.alias === ID) dfd.resolve(DATA)
                });
                return dfd.promise
            }
        }
    }])

    .service('RSSService', function ($q) {
        return {
            RSS: [
                {
                    id: '1',
                    link: 'http://apostrophe.ua/site/newsfeed40',
                    name: 'Новости',
                    image: false
                },
                {
                    id: '2',
                    link: 'http://apostrophe.ua/site/categoryfeed/?alias=politics',
                    name: 'Политика',
                    image: false
                },
                {
                    id: '3',
                    link: 'http://apostrophe.ua/site/categoryfeed/?alias=business',
                    name: 'Бизнес',
                    image: false
                },
                {
                    id: '4',
                    link: 'http://apostrophe.ua/site/categoryfeed/?alias=economy',
                    name: 'Экономика и финансы',
                    image: false
                },
                {
                    id: '5',
                    link: 'http://apostrophe.ua/site/categoryfeed/?alias=society',
                    name: 'Общество',
                    image: false
                },
                {
                    id: '6',
                    link: 'http://apostrophe.ua/site/categoryfeed/?alias=sport',
                    name: 'Спорт',
                    image: false
                },
                {
                    id: '7',
                    link: 'http://apostrophe.ua/site/categoryfeed/?alias=world',
                    name: 'Мир',
                    image: false
                },
                {
                    id: '8',
                    link: 'http://apostrophe.ua/site/articlefeed',
                    name: 'Статьи',
                    image: true
                },
                {
                    id: '9',
                    //link: 'http://apostrophe.ua/site/articlefeed',
                    link: 'http://apostrophe.ua/site/mainfeed',
                    name: 'Лента',
                    image: true
                }
            ],
            getRSS: function () {
                return this.RSS
            },
            getRSS: function (RSSId) {
                var dfd = $q.defer();
                this.RSS.forEach(function (RSS) {
                    if (RSS.id === RSSId) dfd.resolve(RSS)
                });

                return dfd.promise
            }
        }
    })


.factory('$localstorage', ['$window', function ($window) {
    return {
        set: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    }
}])

.factory('DownloadImg',function($q,$cordovaFile){
  var result ={
    fp : null,
    num : null
  };

  function download(URL, Folder_Name, File_Name,i) {
    result.num = i;
    var dfd = $q.defer();
    //step to request a file system
    var download_link = encodeURI(URL);
    ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL

    var directoryEntry = cordova.file.dataDirectory; // to get root path of directory
    $cordovaFile.createDir(directoryEntry,Folder_Name,false);
    var fp = directoryEntry;

    fp = fp + Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give
    result.fp = fp;

    // download function call
    filetransfer(download_link, fp,dfd);
    return dfd.promise;
  }

  function filetransfer(download_link, fp,dfd) {
    var fileTransfer = new FileTransfer();
    // File download function with URL and local path
    fileTransfer.download(download_link, fp,
      function (entry) {
      },
      function (error) {
      }
    );
    dfd.resolve(result);
  }

  function makeid() {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  return {
    storeImg: download,
    name: makeid
  }
});
