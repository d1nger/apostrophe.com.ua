<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
include 'simple_html_dom.php';
// Create DOM from URL
$i=1;
$html = file_get_html($_GET['q']);
// Find all article blocks
foreach($html->find('article.article') as $article){
	$yo['title'] = $article->find('h1[itemprop=headline name]',0)->plaintext;
  $yo['author'] = $article->find('.article__author',0)->plaintext;
	$yo['date'] = $article->find('.article__date',0)->plaintext;
  $art = $article->find('div[itemprop=articleBody]',0);
	foreach($art->find('p') as $par){
		$parnum = 'par'.$i;
		foreach($par->find('iframe') as $frame){
		    $frame->width =null;
		    //$frame->height=null;
		    $frame->style ="width: 100%;";
		}
		$temp[$parnum] = strip_tags($par,'<img><iframe><strong><p>');
		foreach($par->find('img') as $img){
      if(strpos($img->src,"http://apostrophe.com.ua"))
        $imagedata = file_get_contents($img->src->plaintext);
      else
        $imagedata = file_get_contents("http://apostrophe.com.ua".$img->src->plaintext);
           // alternatively specify an URL, if PHP settings allow
      $base64 = base64_encode($imagedata);
      $ext = explode(".",$img->src->plaintext);
		  $temp[$parnum] = '<img data-ng-src="data:image/'.end($ext).';base64,'.$base64.'"/>' ;
		}
		$i = $i +1;
	}
	$yo['article'] = $temp;
	//$yo['article'] = htmlspecialchars((string)$article,ENT_QUOTES); //for better formating
	}
echo json_encode($yo);
?>
