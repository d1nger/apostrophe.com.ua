angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})
.controller("FeedController", function($http, $scope) {

  $scope.init = function() {
    $http.get("http://rss2json.com/api.json", { params: { "rss_url": "http://apostrophe.com.ua/site/newsfeed" } })
      .success(function(data) {
        $scope.rssTitle = data.feed.title;
        $scope.rssUrl = data.feed.link;
        $scope.entries = data.items;
        window.localStorage["entries"] = JSON.stringify(data.responseData.feed.entries);
      })
      .error(function(data) {
        console.log("ERROR: " + data);
        if(window.localStorage["entries"] !== undefined) {
          $scope.entries = JSON.parse(window.localStorage["entries"]);
        }
      });
  }
  /*$scope.init = function() {
   $http.get("https://query.yahooapis.com/v1/public/yql", { params: { "q": "select * from html where url=\'http://blog.nraboy.com/feed/\'","format":"json","env":"store://datatables.org/alltableswithkeys" } })
   .success(function(data) {
   $scope.entries = data.query.results.body.rss.channel.item;
   })
   .error(function(data) {
   console.log("ERROR: " + data);
   });
   }*/
  $scope.browse = function(v) {
    window.open(v, "_system", "location=yes");
  }
});

function xmlToJson(xml) {
	// Create the return object
	var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj["@attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
};
