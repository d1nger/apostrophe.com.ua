<?php
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
include 'simple_html_dom.php';
// Create DOM from URL
$i=1;
$html = file_get_html($_GET['q']);
// Find all article blocks
foreach($html->find('article.article') as $article){
	$yo['title'] = $article->find('h1',0)->plaintext;
	$yo['date'] = $article->find('span',0)->plaintext;
	foreach($article->find('p') as $par){
		$parnum = 'par'.$i;
		$temp[$parnum] = $par->plaintext;
		foreach($par->find('img') as $img){
		    $temp[$parnum] = $img->src;
		}
		$i = $i +1;
	}
	$yo['article'] = $temp;
	//$yo['article'] = htmlspecialchars((string)$article,ENT_QUOTES); //for better formating
	}
echo json_encode($yo);
?>
